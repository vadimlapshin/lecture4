package ru.sbp.springMVC.person;




public class Person implements Comparable<Person>{


    private String name;


    private String city;


    private int age;


    private String email;

    public Person(String name, String city, int age) {
        if (name != null) this.name = name;
        else throw new IllegalArgumentException("The name parameter cannot be null!");
        if (city != null) this.city = city;
        else throw new IllegalArgumentException("The city parameter cannot be null!");
        this.age = age;
    }

    public Person() {

    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Метод возвращает name
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Метод возвращает city
     * @return
     */
    public String getCity() {
        return city;
    }

    /**
     * Метод возвращает age
     * @return
     */
    public int getAge() {
        return age;
    }

    /**
     * Метод, сравнивающий объекты
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || o.getClass() != this.getClass())
            return false;
        Person personT = (Person) o;
        return this.getAge() == personT.getAge() &&
                this.getName().equals(personT.getName()) &&
                this.getCity().equals(personT.getCity());
    }

    @Override
    public int hashCode() {
        int hash = 31;
        hash = hash * 17 + name.hashCode();
        hash = hash * 17 + city.hashCode();
        hash = hash * 17 + age;
        return hash;
    }

    /**
     * Метод, представляющий объект в человеко-читаемом виде
     * @return
     */
    @Override
    public String toString() {
        return "PersonT{" +
                "name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", age=" + age +
                '}';
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     *
     *
     * @param o the object to be compared.
     * @return a negative integer, zero, or a positive integer as this object
     * is less than, equal to, or greater than the specified object.
     * @throws NullPointerException if the specified object is null
     * @throws ClassCastException   if the specified object's type prevents it
     *                              from being compared to this object.
     */
    @Override
    public int compareTo(Person o) {
        if (this.getCity().compareTo(o.getCity()) == 0) {
            return this.getName().compareTo(o.getName());
        } else {
            return this.getCity().compareTo(o.getCity());
        }
    }
}
