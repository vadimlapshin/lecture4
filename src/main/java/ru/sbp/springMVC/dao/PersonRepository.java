package ru.sbp.springMVC.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import ru.sbp.springMVC.person.Person;


import java.util.List;

@Component
@PropertySource("classpath:db.properties")
public class PersonRepository {

    private String url;

    // Здесь показано условно для ДЗ (см. файл конфиг)
    @Value("${url}")
    public void setUrl(String url) {
        this.url = url;
    }

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PersonRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * The method creates a "personsTable" table in the database with the fields "name", "city" and "age".
     */
    public synchronized void createTable() {
        jdbcTemplate.update("CREATE TABLE personsTable (name varchar(32), city varchar(32),age int)");
    }

    /**
     * The method removes the "personTable" table from the database.
     */
    public synchronized void dropTable() {
            jdbcTemplate.update("DROP TABLE personsTable");
        }

    /**
     * The method adds an object of type Person to the personsTable table in the database.
     * @param person
     */
    public synchronized void addPerson(Person person) {
        jdbcTemplate.update("INSERT INTO personsTable VALUES (?,?,?)", person.getName(),
                person.getCity(), person.getAge());

    }

    /**
     * Method for replacing the city value in the name string.
     * @param name
     * @param city
     */
    public synchronized void changeCity(String name, String city) {
        jdbcTemplate.update("UPDATE personsTable SET city=? WHERE name=?", city, name);
    }

    /**
     * Method for finding all elements of the persons table. Returns PersonT as a list.
     * @return
     */
    public synchronized List<Person> findAll() {
        return jdbcTemplate.query("SELECT * FROM personsTable",
                new BeanPropertyRowMapper<>(Person.class));
    }

    /**
     * Method for searching a row in the "personsTable" table by name. Returns a Person object.
     *
     * @param name
     * @return: Person
     */
    public synchronized Person findByName(String name) {
        return  jdbcTemplate.query("SELECT * FROM personsTable WHERE name=?", new Object[]{name},
                new BeanPropertyRowMapper<>(Person.class)).stream().findAny().orElse(null);
    }

    /**
     * Method for deleting a row in a database table by the given field name.
     *
     * @param name
     */
    public synchronized void deleteByName(String name) {
        jdbcTemplate.update("DELETE FROM personsTable WHERE name=?", name);
    }
}
