package ru.sbp.springMVC.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.sbp.springMVC.dao.PersonRepository;
import ru.sbp.springMVC.person.Person;


@Controller
@RequestMapping("/people")
public class PeopleController {

    private final PersonRepository personDao;

    @Autowired
    public PeopleController(PersonRepository personDao) {
        this.personDao = personDao;
    }

    /**
     * Метод выводит список persons из базы данных.
     * Посредством DAO получается список, который помещается в модель.
     * Возвращает страницу HTML со списком persons.
     *
     * @param
     * @return
     */
    @GetMapping()
    public String index(Model model) {
        model.addAttribute("people", personDao.findAll());
        return "people/index";
    }

    /**
     * Метод получения Person по полю "name" из базы данных.
     * Из адреса извлекается "name".
     * Посредством DAO получается объект, который передается в модель.
     * Возвращается страница объекта Person с указанием его полей.
     *
     * @param
     * @param
     * @return
     */
    @GetMapping("/{name}")
    public String show(@PathVariable("name") String name, Model model) {
        model.addAttribute("person", personDao.findByName(name));
        return "people/show";
    }

    /**
     * Метод возвращает HTML страницу с формой для создания нового Person.
     * Создается экземпляр Person с полями по умолчанию и передается в модель.
     *
     * @param
     * @return
     */
    @GetMapping("/new")
    public String newPerson(Model model) {
        model.addAttribute("person", new Person());
        return "people/new";
    }

    /**
     * Метод создает нового Person и добавляет его в базу данных.
     * Из формы извлекаются данные, создается новый Person.
     * Посредством DAO созданный Person добавляется в базу данных.
     * Возвращается страница со списком Person в базе данных.
     *
     * @param
     * @param
     * @return
     */
    @PostMapping()
    public String create(@ModelAttribute("person") Person person) {
        personDao.addPerson(person);
        return "redirect:/people";
    }

    /**
     * Метод возвращает HTML-страницу с формой для редактирования поля «city».
     * В метод внедряется модель, из адреса запроса извлекается «name».
     * В модель вводится человек с полем «name».
     *
     * @param model
     * @param name
     * @return
     */
    @GetMapping("/{name}/edit")
    public String edit(Model model, @PathVariable("name") String name) {
        model.addAttribute("person", personDao.findByName(name));
        return "people/edit";
    }

    /**
     * Метод производит замену значения поля "city" в таблице базы данных по полю "name".
     * Принимается значение поля "city" из формы введенной в HTML странице.
     * Из адреса запроса извлекается "name".
     * Посредством DAO производится замена значения в базе данных.
     * Возвращает страницу HTML со списком персон.
     *
     * @param
     * @param
     * @return
     */
    @PatchMapping("/{name}")
    public String update(@ModelAttribute("person") Person person,
                         @PathVariable("name") String name) {
        personDao.changeCity(name, person.getCity());
        return "redirect:/people";
    }

    /**
     * Метод удаляет данные по полю "name" из базы данных.
     * Из адреса извлекается name.
     * Посредством DAO производится удаление.
     * Возвращает страницу HTML со списком персон.
     *
     * @param name
     * @return
     */
    @DeleteMapping("/{name}")
    public String delete(@PathVariable("name") String name) {
        personDao.deleteByName(name);
        return "redirect:/people";
    }

    /**
     * Метод обрабатывает запрос и возвращает HTML-страницу "О веб-разработчике"
     *
     * @return
     */
    @GetMapping("/about")
    public String about() {
        return "people/about";
    }
}
