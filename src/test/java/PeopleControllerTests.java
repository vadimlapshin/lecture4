import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.sbp.springMVC.controllers.PeopleController;
import ru.sbp.springMVC.dao.PersonRepository;
import ru.sbp.springMVC.person.Person;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;


public class PeopleControllerTests {

    @Mock
    private PersonRepository personDao;

    @InjectMocks
    private PeopleController peopleController;

    private MockMvc mockMvc;

    @Before
    public void before() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(peopleController).build();
        System.out.println("Я вообще работаю!");
    }

    /**
     * Тест на успешность работы метода .index().
     *
     * @throws Exception
     */
    @Test
    public void index_Test() throws Exception {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person());
        personList.add(new Person());
        Mockito.when(personDao.findAll()).thenReturn(personList);
        mockMvc.perform(get("/people"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("people/index"))
                .andExpect(MockMvcResultMatchers.model().attribute("people", hasSize(2)));
    }

    /**
     * Тест на успешность работы метода .show().
     *
     * @throws Exception
     */
    @Test
    public void show_Test() throws Exception {
        String name = "Jerry";
        Mockito.when(personDao.findByName(name)).thenReturn(new Person());
        mockMvc.perform(get("/people/Jerry"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("people/show"))
                .andExpect(MockMvcResultMatchers.model().attribute("person", instanceOf(Person.class)));
    }

    /**
     * Тест на успешность работы метода .newPerson();
     *
     * @throws Exception
     */
    @Test
    public void newPerson_Test() throws Exception {
        mockMvc.perform(get("/people/new"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("people/new"))
                .andExpect(MockMvcResultMatchers.model().attribute("person", instanceOf(Person.class)));
    }

    /**
     * Тест на успешность работы метода .create().
     *
     * @throws Exception
     */
    @Test
    public void create_Test() throws Exception {
        Mockito.verifyNoInteractions(personDao);
        mockMvc.perform(get("/people"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("people/index"));
    }

    /**
     * Тест на успешность работы метода .edit().
     *
     * @throws Exception
     */
    @Test
    public void edit_Test() throws Exception {
        Mockito.verifyNoInteractions(personDao);
        mockMvc.perform(get("/people/Jerry/edit"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("people/edit"));
    }

    /**
     * Тест на успешность работы метода .update().
     */
    @Test
    public void update_Test() throws Exception {
        Mockito.verifyNoInteractions(personDao);
        mockMvc.perform(get("/people"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("people/index"));
    }

    /**
     * Тест на успешность работы метода .delete().
     *
     * @throws Exception
     */
    @Test
    public void delete_Test() throws Exception {
        Mockito.verifyNoInteractions(personDao);
        mockMvc.perform(get("/people/Jerry"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("people/show"));
    }

    /**
     * Метод обрабатывает запрос и возвращает HTML-страницу "О веб-разработчике"
     *
     * @return "/about"
     */
    @Test
    public void about() throws Exception {
        mockMvc.perform(get("/people/about"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("people/about"));
    }
}
